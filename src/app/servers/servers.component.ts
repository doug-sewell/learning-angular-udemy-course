import { Component, OnInit } from '@angular/core';

@Component({
  //selector: '[app-servers]',
  //selector: '.app-servers',
  selector:'app-servers',
  templateUrl:'./servers.component.html',
  styleUrls: ['./servers.component.css']
})
export class ServersComponent implements OnInit {

  allowNewServer:boolean = false;
  serverCreated: String = 'No server was created.';
  serverName: String = 'Testserver';
  newServer:boolean = false;

  constructor() {
    setTimeout(() => {
      this.allowNewServer = true;
    },2000); 
   }

  ngOnInit() {
  }

  createNewServer() {
    this.newServer = true;
    this.serverCreated = 'New server was created! Name is ' + this.serverName;
  }

  onUpdateServerName(event: any) {
    this.serverName = (<HTMLInputElement>event.target).value;
  }

}
