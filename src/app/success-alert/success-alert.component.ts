import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-success',
  template: `
                <div class="alert alert-success">
                Success!
                </div>`
})
export class SuccessAlertComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
